{
  description = "Basic Web training materials";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
      };
    in {
      devShells.${system}.default = pkgs.mkShell {
        name = "basic-web-training-development-shell";
        packages = with pkgs; [
          httpie
          jq
          python3
        ];
      };
    };
}
